import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenSharingService } from '../services/token-sharing.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(private tokenService: TokenSharingService) { }

  tokenObj!: any;
  canActivate(
    route: ActivatedRouteSnapshot,

  ) {
    this.tokenObj = this.tokenService.getToken();
    if (this.tokenObj.role === route.data['role']) {

      console.log(this.tokenObj.role);
      return true;
    }
    return false;
  }

}
