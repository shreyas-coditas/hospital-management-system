import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';

import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'admin', loadChildren: () => import('./modules/admin/admin.module').then(n => n.AdminModule), canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_ADMIN' } },

  { path: 'doctor', loadChildren: () => import('./modules/doctor/doctor.module').then(n => n.DoctorModule), canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_DOCTOR' } },

  { path: 'nurse', loadChildren: () => import('./modules/nurse/nurse.module').then(n => n.NurseModule), canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_NURSE' } },


]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
