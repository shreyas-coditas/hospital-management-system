import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ILogin } from '../auth.types';
import { TokenSharingService } from './token-sharing.service';
@Injectable({
  providedIn: 'any'
})
export class AuthService {

  constructor(private http:HttpClient,private tokenService:TokenSharingService) { }
  headers={
    "ngrok-skip-browser-warning":"1234", 
  }

  proceedLogin(loginCredentials:any){
    return this.http.post('api/auth/signin',loginCredentials,{'headers':this.headers})
    
  }
  
  authenticate(loginCredentials:ILogin){
    return this.http.post('/login',loginCredentials,{'headers':this.headers})
  }

  isLoggedIn(){
    if(this.tokenService.getToken()){
      return true;
    }
    else{
      return false;
    }
  }

  forgotPassword(credetials:any){
    return this.http.post('api/credentials/forgotPassword',credetials,{'headers':this.headers})
  }
  resetPassword(credetials:any){
    return this.http.post('api/credentials/resetPassword',credetials,{'headers':this.headers})
  }
}
