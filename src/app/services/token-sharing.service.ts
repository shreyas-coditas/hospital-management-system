import { Injectable } from '@angular/core';
import { IAuthResponse } from '../types';


@Injectable({
  providedIn: 'any'
})
export class TokenSharingService {

  constructor() { }
  tokenObj!:IAuthResponse;

  setToken(object:any){
    this.tokenObj=object;
    console.log(this.tokenObj);
  }
  getToken(){
    console.log(this.tokenObj)
    return this.tokenObj;
  }
}
