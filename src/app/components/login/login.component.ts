import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { IAuthResponse } from 'src/app/auth.types';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalFormComponent } from 'src/app/modules/shared/components/modal-form/modal-form.component';
import { AuthService } from 'src/app/services/auth.service';
import { TokenSharingService } from 'src/app/services/token-sharing.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private auth: AuthService,
    private tokenService: TokenSharingService,
    private router: Router,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  loginCredentials = this.formBuilder.group({
    "usernameOrEmail": ['', [Validators.email, Validators.required]],
    "password": ['', [Validators.required, Validators.minLength(5)]]
  })

  response!: IAuthResponse;
  role!: string;

  onLogin() {
    console.log(this.loginCredentials)
    if (this.loginCredentials.valid) {
      this.auth.proceedLogin(this.loginCredentials.value).subscribe({
        next: (response) => {
          console.log(response);
          this.tokenService.setToken(response);
          this.role = this.tokenService.getToken().role;
          if (this.role === 'ROLE_ADMIN') {
            this.router.navigate(['admin']);

          }
          else if (this.role === 'ROLE_DOCTOR') {
            this.router.navigate(['doctor']);

          }
          else {
            this.router.navigate(['nurse'])

          }
          this.snackBar.open('Your Are Now Logged In')
        },
        error: (error) => {
          console.log(error.message);
          this.snackBar.open("Invalid Credentials");
        }
      })
    }


  }


  forgotPasswordForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
  });
  forgotPasswordFormFields = [
    { label: "email", inputType: "email", placeholder: "Enter The Email", value: "", controlName: "email" },

  ]

  resetPasswordForm = this.formBuilder.group({
    username: ['', Validators.required],
    oldPassword: ['', [Validators.required, Validators.email]],
    newPassword: ['', Validators.required],
  });
  resetPasswordFormFields = [
    { label: "Email", inputType: "email", placeholder: "Enter The Email", value: "", controlName: "username" },
    { label: "Old Password", inputType: "password", placeholder: "Enter The Old Password", value: "", controlName: "oldPassword" },
    { label: "New password", inputType: "password", placeholder: "Enter The New Password", value: "", controlName: "newPassword" },
  ]

  openForgotPasswordDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {

      title: 'Set New Password',
      form: this.forgotPasswordForm,
      fields: this.forgotPasswordFormFields
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.forgotPassword(result.value);
      this.snackBar.open("New password has been sent you registered Email Id");
    });
  }
  forgotPassword(credetials: any) {
    this.auth.forgotPassword(credetials).subscribe({
      next: (reponse) => {

      }
    })
  }
  openResetPasswordDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {

      title: 'Reset Password',
      form: this.resetPasswordForm,
      fields: this.resetPasswordFormFields
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.resetPassword(result.value);
      this.snackBar.open("Your Password Has Been Reset");
    });
  }
  resetPassword(credentials: any) {
    this.auth.resetPassword(credentials).subscribe();
  }
}
