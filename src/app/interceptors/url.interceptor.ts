import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class UrlInterceptor implements HttpInterceptor {

  constructor() {}
  #baseUrl=environment.baseUrl;
  intercept(request: HttpRequest<unknown>, next: HttpHandler){

    const newRequest=request.clone({
      url:this.#baseUrl+request.url
    })
    return next.handle(newRequest);
  }
}
