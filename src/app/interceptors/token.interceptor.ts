import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenSharingService } from '../services/token-sharing.service';
import { IAuthResponse } from '../types';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private tokenService:TokenSharingService) {}
  token:string='';
  intercept(request: HttpRequest<unknown>, next: HttpHandler){
    if(this.tokenService.getToken()){
      this.token=this.tokenService.getToken().accessToken;
    }
   
    console.log(this.token)
    let newRequest=request.clone({
      setHeaders: { Authorization: `Bearer ${this.token}` }
    })
    return next.handle(newRequest);
    
  }
}
