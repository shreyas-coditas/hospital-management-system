import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from '../shared/components/profile/profile.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminProfileComponent } from './components/admin-profile/admin-profile.component';
import { DoctorDetailsComponent } from './components/doctor-details/doctor-details.component';
import { DoctorRequestsComponent } from './components/doctor-requests/doctor-requests.component';
import { HistoryComponent } from './components/history/history.component';
import { NurseDetailsComponent } from './components/nurse-details/nurse-details.component';
import { NurseRequestsComponent } from './components/nurse-requests/nurse-requests.component';
import { RequestsComponent } from './components/requests/requests.component';

const routes: Routes = [{path:'',component:AdminDashboardComponent,children:[
  {path:'doctor',component:DoctorDetailsComponent},
  {path:'nurse',component:NurseDetailsComponent},
  {path:'profile',component:AdminProfileComponent},
  {path:'requests',component:RequestsComponent,children:[
    {path:'doctorRequests',component:DoctorRequestsComponent},
    {path:'nurseRequests',component:NurseRequestsComponent}
  ]}
  ,{path:'history',component:HistoryComponent}
]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
