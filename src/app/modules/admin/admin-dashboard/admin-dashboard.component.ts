import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenSharingService } from 'src/app/services/token-sharing.service';

import { AdminDataService } from '../services/admin-data.service';


@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {

  constructor(
    private data: AdminDataService,
    private tokenService: TokenSharingService,
    private router: Router) { }

  ngOnInit(): void {

  }

  logOut() {
    this.tokenService.setToken(null);
    this.router.navigate(['']);
  }
}
