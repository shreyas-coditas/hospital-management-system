import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { DoctorDetailsComponent } from './components/doctor-details/doctor-details.component';
import { NurseDetailsComponent } from './components/nurse-details/nurse-details.component';
import { AdminProfileComponent } from './components/admin-profile/admin-profile.component';
import { ContainerComponent } from '../shared/components/container/container.component';

import { RequestsComponent } from './components/requests/requests.component';
import { DoctorRequestsComponent } from './components/doctor-requests/doctor-requests.component';
import { NurseRequestsComponent } from './components/nurse-requests/nurse-requests.component';
import { HistoryComponent } from './components/history/history.component';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AdminDashboardComponent,
    DoctorDetailsComponent,
    NurseDetailsComponent,
    AdminProfileComponent,
   RequestsComponent,
   DoctorRequestsComponent,
   NurseRequestsComponent,
   HistoryComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    HttpClientModule,
    
  ],
  providers:[
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2000}}
  ]
})
export class AdminModule { }
