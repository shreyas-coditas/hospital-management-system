import { Component, OnInit } from '@angular/core';
import { IAcceptReq, IDoctorRequest } from 'src/app/types';
import { DoctorDataService } from '../../services/doctor-data.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ModalFormComponent } from 'src/app/modules/shared/components/modal-form/modal-form.component';
@Component({
  selector: 'app-doctor-requests',
  templateUrl: './doctor-requests.component.html',
  styleUrls: ['./doctor-requests.component.scss']
})
export class DoctorRequestsComponent implements OnInit {

  constructor(
    private doctorData: DoctorDataService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar) { }
  doctorRequestColumns = ['requestId', 'doctorName', 'oldNurseName', 'newNurseName', 'replaceReason', 'status', 'actions']


  doctorRequestData!: IDoctorRequest[]
  ngOnInit(): void {
    this.doctorData.getDoctorRequests().subscribe({
      next: (response: IDoctorRequest[]) => {
        this.doctorRequestData = response;
        console.log(this.doctorRequestData);
      },
      error: (error) => {
        console.log(error.message);
      }
    })

  }


  requestForm = this.formBuilder.group({
    requestId: ['', Validators.required],
    acceptOrDeclineReason: ['', Validators.required],
  });


  requestFormField = [
    { label: "Request Id", inputType: "text", placeholder: "", value: "", controlName: "requestId" },
    { label: "Discription", inputType: "textarea", placeholder: "Enter The Reason", value: "", controlName: "acceptOrDeclineReason" },
  ]
  openDialog = (element: any, callback: any) => {
    this.requestForm.controls['requestId'].setValue(element.requestId);
    this.requestForm.controls['acceptOrDeclineReason'].setValue(element.acceptOrDeclineReason);
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      id: 1,
      title: 'Request',
      form: this.requestForm,
      fields: this.requestFormField,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {

      console.log(result);
      callback(result.value)
    });


  }
  approve = (request: IAcceptReq) => {
    this.doctorData.approveDoctorRequest(request).subscribe({
      next: (response) => {
        console.log('accepted');
        this._snackBar.open("Request was approved!");
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    });
  }

  decline = (request: IAcceptReq) => {
    this.doctorData.declineDoctorRequest(request).subscribe({
      next: (response) => {
        console.log('accepted');
        this._snackBar.open("Request was Declined");
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    });
  }

  approveDialog = (element: any) => {

    this.openDialog(element, this.approve)
  }

  declineDialog = (element: any) => {

    this.openDialog(element, this.decline)

  }

  actions = [
    { name: 'Approve', action: this.approveDialog },
    { name: 'Decline', action: this.declineDialog },
  ]
}
