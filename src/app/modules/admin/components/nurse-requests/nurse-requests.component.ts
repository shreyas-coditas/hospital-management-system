import { Component, OnInit } from '@angular/core';
import { NurseDataService } from '../../services/nurse-data.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IAcceptReq, INurseRequests } from 'src/app/types';
import { ModalFormComponent } from 'src/app/modules/shared/components/modal-form/modal-form.component';
@Component({
  selector: 'app-nurse-requests',
  templateUrl: './nurse-requests.component.html',
  styleUrls: ['./nurse-requests.component.scss']
})
export class NurseRequestsComponent implements OnInit {

  constructor(
    private nurseData: NurseDataService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,

  ) { }

  nurseRequestColumns = ['requestId', 'nurseName', 'oldDoctorName', 'newDoctorName', 'replaceReason', 'status', 'actions']
  declineRequest = () => {
  }

  nurseRequestData!: INurseRequests[]
  ngOnInit(): void {
    this.nurseData.getNurseRequests().subscribe({
      next: (response: INurseRequests[]) => {
        this.nurseRequestData = response;
        console.log(this.nurseRequestData);

      },
      error: (error) => {
        console.log(error.message);
      }
    })

  }


  acceptRequestForm = this.formBuilder.group({
    requestId: ['', Validators.required],
    acceptOrDeclineReason: ['', Validators.required],
  });


  acceptRequestFormField = [
    { label: "Request Id", inputType: "text", placeholder: "", value: "", controlName: "requestId" },
    { label: "Discription", inputType: "textarea", placeholder: "Enter The Reason", value: "", controlName: "acceptOrDeclineReason" },
  ]
  openDialog = (element: any, callback: any) => {
    this.acceptRequestForm.controls['requestId'].setValue(element.requestId);
    this.acceptRequestForm.controls['acceptOrDeclineReason'].setValue(element.acceptOrDeclineReason);
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      id: 1,
      title: 'Approve Request',
      form: this.acceptRequestForm,
      fields: this.acceptRequestFormField,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {

      console.log(result);
      callback(result.value)
    });


  }
  approve=(request: IAcceptReq)=>{

    this.nurseData.approveNurseRequest(request).subscribe({
      next: (response) => {
        console.log('approved')
      },
      error:()=>{},
      complete:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
        this._snackBar.open('Request Was Approved');
      }
    });
  }
  decline=(request: IAcceptReq)=>{
    this.nurseData.declineNurseRequest(request).subscribe({
      next: (response) => {
        console.log('accepted');
      },
      error:()=>{},
      complete:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)

        this._snackBar.open("Request was Declined");
      }
    });
  }

  approveDialog = (element: any) => {
   
    this.openDialog(element, this.approve)

  }

  declineDialog = (element: any) => {

    this.openDialog(element, this.decline)
    
  }
  actions = [
    { name: 'Approve', action: this.approveDialog },
    { name: 'Decline', action: this.declineDialog },
  ]

}



