import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { IDoctor, IDoctorResponse, IDoctorListItem, INurseResponse, IAddDoctor, IUpdateDoctor } from 'src/app/types';
import { Location } from '@angular/common';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { ModalFormComponent } from 'src/app/modules/shared/components/modal-form/modal-form.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { DoctorDataService } from '../../services/doctor-data.service';
import { NurseDataService } from '../../services/nurse-data.service';
import { ModalComponent } from 'src/app/modules/shared/components/modal/modal.component';
@Component({
  selector: 'app-doctor-details',
  templateUrl: './doctor-details.component.html',
  styleUrls: ['./doctor-details.component.scss']
})
export class DoctorDetailsComponent implements OnInit {

  constructor(
    private data: DoctorDataService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private router: Router,
    private nurseData: NurseDataService,
    private location: Location,
    private snackBar:MatSnackBar) { }



  editDoctorForm = this.formBuilder.group({
    doctorName: ['', Validators.required],
    specialization: ['', Validators.required],
  });


  editDoctorFormFileds = [
    { label: "Name", inputType: "text", placeholder: "Enter Docotor Name", value: "", controlName: "doctorName" },
    { label: "Specialization", inputType: "text", placeholder: "Enter The Specialization", value: "", controlName: "specialization" },
  ]
  openEditDialog = (element: any) => {
    this.editDoctorForm.controls['doctorName'].setValue(element.doctorName);
    this.editDoctorForm.controls['specialization'].setValue(element.specialization);
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      title: 'Edit Doctor',
      form: this.editDoctorForm,
      fields: this.editDoctorFormFileds,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {

      console.log(result);//returns undefined
      this.editDoctor(element.doctorId, result.value)
    });

  }
  editDoctor(id: number, changedData: IUpdateDoctor) {
    this.data.updateDoctorData(id, changedData).subscribe({
      next: () => {
        this.snackBar.open(`Doctor Id ${id} Was Edited Succesfully`);  
      },
      error:()=>{},
      complete:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    })
  }

  doctors!: IDoctorResponse[];
  unallocatedNurses!: INurseResponse[];
  doctorColumns: string[] = ['doctorId', 'doctorName', 'email', 'specialization', 'actions'];

  openDeleteDialog = (element: any) => {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      
      title: 'Delete Doctor',
      message: 'Are You Sure You Want To Delete?',
      action1: 'Yes',
      action2: 'No'
    };

    const dialogRef = this.dialog.open(ModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {

      console.log(result);
      if (result === true) {
        this.deleteDoctor(element)
      }
    });

  }
  deleteDoctor = (row: any) => {
    console.log(row.doctorId);
    this.data.deleteDoctorData(row.doctorId).subscribe({
      next: () => {
        this.snackBar.open('Doctor Was Deleted Succesfully');
      },
      error:()=>{

      },complete:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }

    });

  }
  actions = [
    { name: 'Edit', action: this.openEditDialog },
    { name: 'Delete', action: this.deleteDoctor },
  ]

  ngOnInit(): void {

  
    this.data.getDoctors().subscribe({
      next: (response: IDoctorResponse[]) => {
        this.doctors = response;
      },
      error: (error) => {
        console.log(error.message);
      }
    })


    this.nurseData.getNurses().subscribe({
      next: (response: INurseResponse[]) => {
        this.unallocatedNurses = response;
      },
      error: (error) => {
        console.log(error.message);
      }
    })

  }





  addDoctorForm = this.formBuilder.group({
    doctorName: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    specialization: ['', Validators.required],
  });


  addDoctorFormFileds = [
    { label: "Name", inputType: "text", placeholder: "Enter Docotor Name", value: "", controlName: "doctorName" },
    { label: "Email", inputType: "email", placeholder: "Enter Docotor Email", value: "", controlName: "email" },
    { label: "Specialization", inputType: "text", placeholder: "Enter The Specialization", value: "", controlName: "specialization" },

  ]

  openAddDoctorDialog = () => {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      id: 1,
      title: 'Add New Doctor',
      form: this.addDoctorForm,
      fields: this.addDoctorFormFileds,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.sendDoctorDetails(result.value);
      
      console.log(result);
    });

  }

  sendDoctorDetails(credetials: IAddDoctor) {
    console.log(credetials);
    this.data.addDoctor(credetials).subscribe({
      next: () => {
        this.snackBar.open('Doctor Was Added Succesfully');
      },
      error: (error) => {
        console.log(error.message)
      },
      complete: () => {
        this.router.navigateByUrl('/admin/doctor', { skipLocationChange: true });
        this.router.navigate([decodeURI(this.location.path())]);
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    })
  }

  nurseId: FormControl = new FormControl([]);
  doctorId: FormControl = new FormControl([]);
  allocateNurseForm = this.formBuilder.group({
    doctorId: this.doctorId,
    nurseId: this.nurseId,
  });
  allocateNurseFormFileds = [
    { label: "Doctor", inputType: "list", placeholder: "", value: "", controlName: "doctorId", index: 0, multiple: false },
    { label: "Nurses", inputType: "list", placeholder: "", value: "", controlName: "nurseId", index: 1, multiple: false }
  ]

  openAllocateDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      
      title: 'Allocate Nurse',
      form: this.allocateNurseForm,
      fields: this.allocateNurseFormFileds,
      lists: [this.doctors, this.unallocatedNurses]
    };
    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      this.allocate(result.value);
      console.log(result);
    });

  }

  allocate(data: any) {
    this.data.allocateNurse(data).subscribe();
    this.snackBar.open('Nurse Was Allocated Succesfully!');
  }

}
