import { Component, OnInit } from '@angular/core';
import { IDoctorHistory, INurseHistory } from 'src/app/types';
import { DoctorDataService } from '../../services/doctor-data.service';
import { NurseDataService } from '../../services/nurse-data.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  constructor(
    private doctorData:DoctorDataService,
    private nurseData:NurseDataService) { }

  doctorHistoryDetails!:IDoctorHistory[];
  nurseHistoryDetails!:INurseHistory[];
  ngOnInit(): void {
    this.doctorData.getDoctorsHistory().subscribe({
      next:(response:IDoctorHistory[])=>{
        this.doctorHistoryDetails=response;
        console.log(this.doctorHistoryDetails)
        this.ngOnInit();
      }
    })

    this.nurseData.getNurseHistory().subscribe({
      next:(response:INurseHistory[])=>{
        this.nurseHistoryDetails=response;
        console.log(this.nurseHistoryDetails)
        this.ngOnInit();
      },
      error:(error)=>{
        console.log(error);
      }
    })

  }

}
