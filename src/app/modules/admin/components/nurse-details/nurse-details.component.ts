import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { INurseResponse, IDoctorResponse } from 'src/app/types';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ModalFormComponent } from 'src/app/modules/shared/components/modal-form/modal-form.component';
import { NurseDataService } from '../../services/nurse-data.service';
import { DoctorDataService } from '../../services/doctor-data.service';
@Component({
  selector: 'app-nurse-details',
  templateUrl: './nurse-details.component.html',
  styleUrls: ['./nurse-details.component.scss']
})
export class NurseDetailsComponent implements OnInit {

  constructor(private data: NurseDataService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private doctorData: DoctorDataService,
    private snackBar: MatSnackBar
  ) { }

  editNurseForm = this.formBuilder.group({
    nurseName: ['', Validators.required],
  });


  editNurseFormFileds = [
    { label: "Nurse Name", inputType: "text", placeholder: "Enter Nurse Name", value: "", controlName: "nurseName" },

  ]
  openEditDialog = (element: any) => {
    this.editNurseForm.controls['nurseName'].setValue(element.nurseName);

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      id: 1,
      title: 'Edit Doctor',
      form: this.editNurseForm,
      fields: this.editNurseFormFileds,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {

      console.log(result);
      this.editNurse(element.nurseId, result.value)
    });

  }
  editNurse(id: number, changedData: any) {
    this.data.updateNurse(id, changedData).subscribe({
      next: () => {
        this.snackBar.open('Nurse Was Edited Succesfully');
        console.log("updated");
      },
      error:()=>{},
      complete:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    })
    // console.log(id);
  }
  deleteNurse = (row: any) => {

    this.data.deleteNurse(row.nurseId).subscribe({
      next: () => {
        this.snackBar.open('Nurse Was Deleted Succesfully!');
      },
      error:()=>{},
      complete:()=>{
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    });

  }
  nurseDetails!: INurseResponse[];
  nurseColumns: string[] = ['nurseId', 'nurseName', 'email', 'doctorDto', 'actions'];

  actions = [
    { name: 'Edit', action: this.openEditDialog },
    { name: 'Delete', action: this.deleteNurse },
  ]

  doctors!: IDoctorResponse[]
  ngOnInit(): void {
    this.data.getNurses().subscribe({
      next: (response: INurseResponse[]) => {
        console.log(response)
        this.nurseDetails = response;
        this.ngOnInit();
      },
      error: (error) => {
        console.log(error);
      }
    })

    this.doctorData.getDoctors().subscribe({
      next: (response: IDoctorResponse[]) => {
        console.log(response);
        this.doctors = response;
        console.log(this.doctors)
      },
      error: (error) => {
        console.log(error.message);
      }
    })
  }


  nurseID: FormControl = new FormControl([]);
  doctorId: FormControl = new FormControl([]);

  addNurseForm = this.formBuilder.group({
    nurseName: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],

  });
  addNurseFormFileds = [
    { label: "Name", inputType: "text", placeholder: "Enter Nurse Name", value: "", controlName: "nurseName" },
    { label: "Email", inputType: "email", placeholder: "Enter Nurse Email", value: "", controlName: "email" },
  ]

  openAddNurseDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      id: 1,
      title: 'Add New Nurse',
      form: this.addNurseForm,
      fields: this.addNurseFormFileds,
    };

    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
     
      console.log(result);//returns undefined
      this.addNurse(result.value)
    });
  }
  addNurse(nurseDetails: any) {
    console.log(nurseDetails);
    this.data.addNewNurse(nurseDetails).subscribe({
      next: () => {
        this.snackBar.open('Nurse Was Added Succesfully');
        setTimeout(()=>{
          this.ngOnInit()
        },300)
      }
    })
  }
  allocateNurseForm = this.formBuilder.group({
    nurseId: this.nurseID,
    doctorId: this.doctorId,
  });
  allocateNurseFormFileds = [
    { label: "Doctor", inputType: "list", placeholder: "", value: "", controlName: "doctorId", index: 0, multiple: false },
    { label: "Nurses", inputType: "list", placeholder: "", value: "", controlName: "nurseId", index: 1, multiple: false }
  ]

  openAllocateDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      id: 1,
      title: 'Allocate Nurse',
      form: this.allocateNurseForm,
      fields: this.allocateNurseFormFileds,
      lists: [this.doctors, this.nurseDetails]
    };
    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {

      console.log(result);
      this.allocateDoctor(result.value)
    });
  }

  allocateDoctor(allocatedDetails: any) {
    this.data.allocateDoctor(allocatedDetails).subscribe();
    this.snackBar.open('Doctor Was Allocated Succesfully');
  }

}
