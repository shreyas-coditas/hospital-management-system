import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IDoctor ,IDoctorResponse,IDoctorAddDetails, INurseResponse, IDoctorRequest, IAcceptReq, IDoctorHistory} from 'src/app/types';
@Injectable({
  providedIn: 'root'
})
export class DoctorDataService {

  constructor(private http:HttpClient) { }
  headers={
    "ngrok-skip-browser-warning":"1234",
    
  }
  getDoctors(){
    return this.http.get<IDoctorResponse[]>('api/hms/doctors',{headers:this.headers})
  }
  addDoctor(doctorDetails:IDoctorAddDetails){
    return this.http.post('api/hms/addDoctor',doctorDetails,{headers:this.headers})
  }
  deleteDoctorData(id:number){
    console.log(id);
    return this.http.delete(`api/hms/deleteDoctor/${id}`,{headers:this.headers})
  }
  updateDoctorData(id:number,data:any){
    console.log(id);
    return this.http.put(`api/hms/updateDoctor/${id}`,data, {headers:this.headers})
  }

  allocateNurse(assignData:any){
    return this.http.post(`api/admin/assignNurseToDoctor`,assignData, {headers:this.headers})
  }

  getDoctorRequests(){
    return this.http.get<IDoctorRequest[]>('api/hms/getDoctorRequest',{headers:this.headers})
  }

  approveDoctorRequest(acceptedData:IAcceptReq){
    return this.http.post('api/admin/acceptDoctorRequest',acceptedData,{headers:this.headers})
  }

  getDoctorsHistory(){
    return this.http.get<IDoctorHistory[]>('api/hms/getDoctorHistory',{headers:this.headers})
  }

  declineDoctorRequest(declineData:any){
    return this.http.post('api/admin/declineDoctorRequest',declineData,{headers:this.headers})
  }
}
