import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IDoctor ,IDoctorResponse,IDoctorAddDetails, INurseResponse, INurseRequests, INurseHistory,IAcceptReq} from 'src/app/types';
@Injectable({
  providedIn: 'root'
})
export class NurseDataService {

  constructor(private http:HttpClient) { }
  headers={
    "ngrok-skip-browser-warning":"1234",
    
  }
   getNurses(){
    return this.http.get<INurseResponse[]>('api/hms/allNurses',{headers:this.headers})
  }
  addNewNurse(nurseData:any){
    return this.http.post('api/hms/addNurse',nurseData,{headers:this.headers})
  }
  updateNurse(id:number,nurseData:any){
    return this.http.put(`api/hms/updateNurse/${id}`,nurseData,{headers:this.headers})
  }
  deleteNurse(id:number){
    return this.http.delete(`api/hms/deleteNurse/${id}`,{headers:this.headers});
  }
  
  allocateDoctor(allocatedData:any){
    return this.http.post(`api/admin/assignDoctorToNurse`,allocatedData,{headers:this.headers});
  }

  getNurseRequests(){
    return this.http.get<INurseRequests[]>('api/hms/getNurseRequest',{headers:this.headers})
  }

  approveNurseRequest(acceptedData:IAcceptReq){
    return this.http.post('api/admin/acceptNurseRequest',acceptedData,{headers:this.headers})
  }

  getNurseHistory(){
    return this.http.get<INurseHistory[]>('api/hms/getNurseHistory',{headers:this.headers})
  }

  declineNurseRequest(declineData:any){
    return this.http.post('api/admin/declineNurseRequest',declineData,{headers:this.headers})
  }
}
