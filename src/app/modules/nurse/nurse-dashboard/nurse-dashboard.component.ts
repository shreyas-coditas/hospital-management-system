import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenSharingService } from 'src/app/services/token-sharing.service';

@Component({
  selector: 'app-nurse-dashboard',
  templateUrl: './nurse-dashboard.component.html',
  styleUrls: ['./nurse-dashboard.component.scss']
})
export class NurseDashboardComponent implements OnInit {

  constructor(
    private tokenService:TokenSharingService,
    private router:Router
    ) { }

  ngOnInit(): void {
  }
  logOut() {
    this.tokenService.setToken(null);
    this.router.navigate(['']);
  }
}
