import { TestBed } from '@angular/core/testing';

import { NursesDataService } from './nurses-data.service';

describe('NursesDataService', () => {
  let service: NursesDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NursesDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
