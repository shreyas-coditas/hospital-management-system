import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorInfoComponent } from './components/doctor-info/doctor-info.component';
import { HistoryComponent } from './components/history/history.component';
import { NurseDashboardComponent } from './nurse-dashboard/nurse-dashboard.component';

const routes: Routes = [{path:'',component:NurseDashboardComponent,children:[
  {path:'doctorDetail',component:DoctorInfoComponent},
  {path:'history',component:HistoryComponent}
]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NurseRoutingModule { }
