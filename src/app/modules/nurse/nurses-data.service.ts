import { HttpClient } from '@angular/common/http';
import { Injectable, NgProbeToken } from '@angular/core';
import { TokenSharingService } from 'src/app/services/token-sharing.service';
import { IDoctorResponse, INurseHistory } from 'src/app/types';

@Injectable({
  providedIn: 'root'
})
export class NursesDataService {

  currectNurseId!: number;
  constructor(
    private tokenService: TokenSharingService,
    private http: HttpClient
  ) { }

  headers = {
    "ngrok-skip-browser-warning": "1234",
  }
  getDoctorDetail() {
    const id = this.tokenService.getToken().id;
    this.currectNurseId = id;
    return this.http.get<IDoctorResponse>(`api/hms/getDoctorByNurseId/${id}`, { headers: this.headers })
  }
  replaceDoctor(replaceData:any){
    
    return this.http.post(`api/hms/replaceDoctor`,replaceData,{headers:this.headers})
  }

  getAllDoctors(){
    return this.http.get<IDoctorResponse[]>(`api/hms/doctors`,{headers:this.headers})
  }
  getNurseId(){
    return this.currectNurseId;
  }

  getNurseHistory(){
    return this.http.get<INurseHistory[]>(`api/hms/getNurseHistory/${this.currectNurseId}`,{headers:this.headers})
  }
}
