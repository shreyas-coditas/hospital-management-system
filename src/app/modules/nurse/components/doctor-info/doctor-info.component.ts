import { Component, OnInit } from '@angular/core';
import { NurseDataService } from 'src/app/modules/admin/services/nurse-data.service';
import { IDoctorResponse } from 'src/app/types';
import { NurseDashboardComponent } from '../../nurse-dashboard/nurse-dashboard.component';
import { NursesDataService } from '../../nurses-data.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalFormComponent } from 'src/app/modules/shared/components/modal-form/modal-form.component';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-doctor-info',
  templateUrl: './doctor-info.component.html',
  styleUrls: ['./doctor-info.component.scss']
})
export class DoctorInfoComponent implements OnInit {

  constructor(
    private nurseData: NursesDataService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar) { }

  allDoctors!: IDoctorResponse[];
  doctorDetails!: IDoctorResponse;
  ngOnInit(): void {
    this.nurseData.getDoctorDetail().subscribe({
      next: (response: IDoctorResponse) => {
        this.doctorDetails = response;
        console.log(response);
      },
      error: (error) => {
        console.log(error.message)
      }
    })



    this.nurseData.getAllDoctors().subscribe({
      next:(response:IDoctorResponse[])=>{
        this.allDoctors=response
        console.log(this.allDoctors);
      },error:(error)=>{
        console.log(error.message);
      }
    })
  }
  newDoctorId: FormControl = new FormControl([]);
  replaceDoctorForm = this.formBuilder.group({
    nurseId: ['', Validators.required],
    oldDoctorId: ['', Validators.required],
    newDoctorId: this.newDoctorId,
    replaceReason: ['', [Validators.required, Validators.email]],

  });
  replaceDoctorFormFileds = [
    { label: "Nurse Id", inputType: "number", placeholder: "", value: "", controlName: "nurseId" },
    { label: "Old Doctor ID", inputType: "number", placeholder: "", value: "", controlName: "oldDoctorId" },
    { label: "New Doctor", inputType: "list", placeholder: "Enter New Nurse", value: "", controlName: "newDoctorId", index: 0, multiple: false },
    { label: "Enter the Reason to replace", inputType: "text", placeholder: "Enter the reason", value: "", controlName: "replaceReason" },
  ]
  replaceDoctorDialog() {
    const doctorID = this.nurseData.getNurseId();

    this.replaceDoctorForm.controls['nurseId'].setValue('' + doctorID);

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {

      title: 'Allocate Nurse',
      form: this.replaceDoctorForm,
      fields: this.replaceDoctorFormFileds,
      lists: [this.allDoctors]
    };
    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {

      console.log(result);
      this.replaceDoctor(result.value)
    });
  }




  replaceDoctor(details: any) {

    this.nurseData.replaceDoctor(details).subscribe({
      next: (response) => {
        console.log('replace request sent');
        this.snackbar.open('Request has been sent')
      }
    })
  }
}
