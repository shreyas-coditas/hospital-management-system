import { Component, OnInit } from '@angular/core';
import { INurseHistory } from 'src/app/types';
import { NursesDataService } from '../../nurses-data.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  constructor(private nurseData:NursesDataService) { }
  nurseHistoryDetails!:INurseHistory[];
  ngOnInit(): void {

    this.nurseData.getNurseHistory().subscribe({
      next:(response:INurseHistory[])=>{
        this.nurseHistoryDetails=response;
        console.log(this.nurseHistoryDetails)
       
      }
    })
  }

}
