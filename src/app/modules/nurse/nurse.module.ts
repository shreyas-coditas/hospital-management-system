import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NurseRoutingModule } from './nurse-routing.module';
import { NurseDashboardComponent } from './nurse-dashboard/nurse-dashboard.component';

import { SharedModule } from '../shared/shared.module';
import { DoctorInfoComponent } from './components/doctor-info/doctor-info.component';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { HistoryComponent } from './components/history/history.component';


@NgModule({
  declarations: [
    NurseDashboardComponent,
   
    DoctorInfoComponent,
        HistoryComponent,
  
  ],
  imports: [
    CommonModule,
    NurseRoutingModule,
    SharedModule
  ],
  providers:[
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2000}}
  ]
})
export class NurseModule { }
