import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenSharingService } from 'src/app/services/token-sharing.service';

@Component({
  selector: 'app-doctor-dashboard',
  templateUrl: './doctor-dashboard.component.html',
  styleUrls: ['./doctor-dashboard.component.scss']
})
export class DoctorDashboardComponent implements OnInit {

  constructor(
    private tokenService:TokenSharingService,
    private router:Router
    ) { }

  ngOnInit(): void {
  }
  logOut(){
    this.tokenService.setToken(null);
    this.router.navigate(['']);
  }
}
