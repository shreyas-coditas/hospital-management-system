import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorRoutingModule } from './doctor-routing.module';
import { DoctorDashboardComponent } from './doctor-dashboard/doctor-dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { NurseDetailsComponent } from './components/nurse-details/nurse-details.component';
import { HistoryComponent } from './components/history/history.component';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';


@NgModule({
  declarations: [
    DoctorDashboardComponent,
    NurseDetailsComponent,
    HistoryComponent
  ],
  imports: [
    CommonModule,
    DoctorRoutingModule,
    SharedModule
  ],
  providers:[
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2000},multi:true}
  ]
})
export class DoctorModule { }
