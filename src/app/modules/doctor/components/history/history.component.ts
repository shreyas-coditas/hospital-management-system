import { Component, OnInit } from '@angular/core';
import { IDoctorHistory } from 'src/app/types';
import { DoctorsDataService } from '../../services/doctors-data.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  constructor(private doctorData:DoctorsDataService) { }
  doctorHistoryDetails!:IDoctorHistory[];
  ngOnInit(): void {
    this.doctorData.getDoctorsHistory().subscribe({
      next:(response:IDoctorHistory[])=>{
        this.doctorHistoryDetails=response;
        console.log(this.doctorHistoryDetails)
       
      }
    })
  }

}
