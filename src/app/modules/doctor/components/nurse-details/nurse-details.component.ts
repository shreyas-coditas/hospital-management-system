import { Component, OnInit } from '@angular/core';
import { IdoctorDetails, INurseResponse } from 'src/app/types';
import { DoctorsDataService } from '../../services/doctors-data.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { ModalFormComponent } from 'src/app/modules/shared/components/modal-form/modal-form.component';
import { TokenSharingService } from 'src/app/services/token-sharing.service';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-nurse-details',
  templateUrl: './nurse-details.component.html',
  styleUrls: ['./nurse-details.component.scss']
})
export class NurseDetailsComponent implements OnInit {

  constructor(
    private dataService:DoctorsDataService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private tokenService:TokenSharingService,
    private snackBar:MatSnackBar
    ) { }
  
  
  nurseDetails!: INurseResponse[];
  nurseColumns: string[] = ['nurseId', 'nurseName', 'email','actions'];
  unAllocatedNurse!:INurseResponse[];
  

  ngOnInit(): void {
    
    this.dataService.getNurseDetails().subscribe({
      next:(response:INurseResponse[])=>{
        this.nurseDetails=response;
        console.log(response);
        
      }
    })


    this.dataService.getUnallocatedNurses().subscribe({
      next:(response:INurseResponse[])=>{
        this.unAllocatedNurse=response
        console.log(this.unAllocatedNurse);
      },error:(error)=>{
        console.log(error.message);
      }
    })
  }
  newNurseId: FormControl = new FormControl([]);
  replaceNurseForm = this.formBuilder.group({
    doctorId: ['', Validators.required],
    oldNurseId: ['', Validators.required],
    newNurseId: this.newNurseId,
    replaceReason: ['', [Validators.required, Validators.email]],

  });
  replaceNurseFormFileds = [
    { label: "Doctor Id", inputType: "number", placeholder: "", value: "", controlName: "doctorId" },
    { label: "Old Nurse ID", inputType: "number", placeholder: "", value: "", controlName: "oldNurseId" },
    { label: "New Nurse", inputType: "list", placeholder: "Enter New Nurse", value: "", controlName: "newNurseId",index: 1, multiple: false },
    { label: "Enter the Reason to replace", inputType: "text", placeholder: "Enter the reason", value: "", controlName: "replaceReason" },
  ]
 
 

  openReplaceDialog=(element:any)=>{
    
    
    const doctorID=this.dataService.getDoctorId();
    console.log(doctorID);
    this.replaceNurseForm.controls['doctorId'].setValue(''+doctorID);
    this.replaceNurseForm.controls['oldNurseId'].setValue(element.nurseId);
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '35vw';
    dialogConfig.data = {
      
      title: 'Allocate Nurse',
      form: this.replaceNurseForm,
      fields: this.replaceNurseFormFileds,
      lists: [[],this.unAllocatedNurse]
    };
    const dialogRef = this.dialog.open(ModalFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {

      console.log(result);
      this.replaceNurse(result.value)
    });
  }

  replaceNurse(details:any) {
  
    this.dataService.replaceNurse(details).subscribe({
      next:(response)=>{
        console.log('replace request sent');
        this.snackBar.open('Request Has Been Sent!')
      }
    })
  }

 
  actions = [
    { name: 'Replace', action: this.openReplaceDialog},
  ]
}
