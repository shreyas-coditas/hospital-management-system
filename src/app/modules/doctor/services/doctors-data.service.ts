import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TokenSharingService } from 'src/app/services/token-sharing.service';
import { IdoctorDetails, IDoctorHistory, INurseResponse } from 'src/app/types';

@Injectable({
  providedIn: 'root'
})
export class DoctorsDataService {

  constructor(private http:HttpClient,private tokenService:TokenSharingService) { }



  headers={
    "ngrok-skip-browser-warning":"1234",
  }
  currectDoctorId!:number;
  getDoctorId(){
    return this.currectDoctorId;
  }
   getDoctoDetails(){
    const id=this.tokenService.getToken().id;
    return this.http.get<IdoctorDetails[]>(`api/hms/getDoctor/${id}`,{headers:this.headers})
  }
  getNurseDetails(){
    const id=this.tokenService.getToken().id;
    this.currectDoctorId=id;
    return this.http.get<INurseResponse[]>(`api/hms/getNurseByDoctorId/${id}`,{headers:this.headers})
  }

  replaceNurse(replaceData:any){
    
    return this.http.post(`api/hms/replaceNurse`,replaceData,{headers:this.headers})
  }

  getUnallocatedNurses(){
    return this.http.get<INurseResponse[]>(`api/hms/unallocatedNurses`,{headers:this.headers})
  }

  getDoctorsHistory(){
    return this.http.get<IDoctorHistory[]>(`api/hms/getDoctorHistory/${this.currectDoctorId}`,{headers:this.headers})
  }
}
