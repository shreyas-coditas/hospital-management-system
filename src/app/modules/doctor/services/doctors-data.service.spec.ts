import { TestBed } from '@angular/core/testing';

import { DoctorsDataService } from './doctors-data.service';

describe('DoctorsDataService', () => {
  let service: DoctorsDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DoctorsDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
