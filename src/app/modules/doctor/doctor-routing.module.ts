import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HistoryComponent } from './components/history/history.component';
import { NurseDetailsComponent } from './components/nurse-details/nurse-details.component';

import { DoctorDashboardComponent } from './doctor-dashboard/doctor-dashboard.component';

const routes: Routes = [{path:'',component:DoctorDashboardComponent,children:[
  {path:'nurses',component:NurseDetailsComponent},
  {path:'doctorHistory',component:HistoryComponent}
]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorRoutingModule { }
