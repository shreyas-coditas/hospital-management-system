import { Component, OnInit,Input ,Inject} from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms'
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data:any,private dialogRef: MatDialogRef<ModalComponent>,) { }

  ngOnInit(): void {
  }
  @Input() modalTitle!:string;
  @Input() form!:FormGroup;
  @Input() formFields!:any;

  close() {
    this.dialogRef.close();
  }
}
