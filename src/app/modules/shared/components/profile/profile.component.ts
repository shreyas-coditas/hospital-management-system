import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  profileDetails={
    id:2,
    name:'Dr. Shreyas Babshet',
    email:'shreyasbabshet@coditas.com',
    specialization:'Heart',
    Nurses:['abc','bac','cdb'],
  }
}
