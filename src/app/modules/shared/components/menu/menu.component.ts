import { Component, OnInit,Input,EventEmitter ,Output} from '@angular/core';
import { FormControl } from '@angular/forms';


interface IList{
  id:number,
  value:string,
}

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
@Input() formControl!:any;
 @Input() formControlName!:string;
 @Input() itemsList!:IList[];
@Input() label!:string;
}
