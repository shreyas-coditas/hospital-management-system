import { Component, AfterViewInit, ViewChild, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table'
import { MatPaginator } from '@angular/material/paginator';
interface IColumn {
  columnName: string,
  type: string
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

  }
  @Input() dataSource!: any;
  @Input() dataColumns!: string[];
  @Input() buttons: any;
  

 
  isOption(column: string) {
    if (column !== 'options') {
      return true
    }
    else {
      return false;
    }
  }
  id!: number | string;
  isId(column: string) {
    if (column === 'id') {
      return true
    }
    return false;
  }

  getId(value: number | string, column: string) {
    if (this.isId(column)) {
      this.id = value;
    }
  }

  isArray(element: any) {
    if (element.isArray) {
      console.log(element)
    }
  }
}
