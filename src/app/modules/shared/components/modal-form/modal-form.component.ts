import { Component, OnInit, Inject,Output, Input ,EventEmitter} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
@Component({
  selector: 'app-modal-form',
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.scss']
})
export class ModalFormComponent implements OnInit {
  formFields!: any;
  form!: FormGroup;
  description: string;

  @Output() formEmitter=new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ModalFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.description = data.description;
    this.form = data.form;
    this.formFields = data.fields;
  }



  

  ngOnInit() {
    console.log(this.form);
    
  }

  onSubmit() {
    this.dialogRef.close();
  }

  close() {
    this.dialogRef.close();
  }
  addToList(item:any){
    this.data.selectedNurses.push(item);
  }

}
