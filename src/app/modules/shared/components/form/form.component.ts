import { Component, OnInit ,Input} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { FormBuilder } from '@angular/forms'
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor(private formBuilder:FormBuilder) { }
  @Input() formGroup!:FormGroup;
  @Input() formFields!:any;
  ngOnInit(): void {
    console.log(this.formGroup.value)
  }
  
  
  display(){
    console.log(this.formGroup.value)
  }
}
