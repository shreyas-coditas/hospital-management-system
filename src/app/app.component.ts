import { Component } from '@angular/core';
import { FormBuilder,FormControl,FormGroup,Validators } from '@angular/forms'
import { MatDialog ,MatDialogConfig} from '@angular/material/dialog';
import { ModalFormComponent } from './modules/shared/components/modal-form/modal-form.component';
import { ModalComponent } from './modules/shared/components/modal/modal.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'hospital-management-system';

  constructor(private formBuilder:FormBuilder,public dialog:MatDialog){

  }
  
  profileForm = this.formBuilder.group({
    name: ['',Validators.required],
    email: ['',Validators.required],
    number:['',Validators.required],
    password:['',Validators.required],
  });
  formFields=[
    {label:"name",inputType:"text",placeholder:"Enter The Name",value:"",controlName:"name"},
    {label:"email",inputType:"email",placeholder:"Enter The Email",value:"",controlName:"email"},
    {label:"Mob Number",inputType:"text",placeholder:"Enter The number",value:"",controlName:"number"},
    {label:"password",inputType:"password",placeholder:"Enter The Password",value:"",controlName:"password"},
  ]

  openDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth='35vw';
    dialogConfig.data = {
        id: 1,
        title: 'Angular For Beginners',
        form:this.profileForm,
        fields:this.formFields
    };

    this.dialog.open(ModalFormComponent, dialogConfig);
}
}

