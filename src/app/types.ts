export interface IAuthResponse {
    accessToken: string,
    role: string,
    id: number

}
export interface IDoctorResponse {
    doctorId: number,
    doctorName: string,
    email: string,
   
    password: any,
    specialization: string,
}

export interface IDoctor {
    doctorId: number,
    doctorName: string,
    email: string,
    nurses: any,
    specialization: string,
}
export interface IDoctorAddDetails {
    doctorName: string,
    email: string,
    specialization: string
}
export interface IAddDoctor{
    "doctorName":string ,
    "email": string,
    "specialization": string
}
export interface IUpdateDoctor{
    "doctorName":string ,
    "specialization": string
}
export interface INurseResponse {
    nurseId: number,
    nurseName: string,
    email: string,
    doctorDto: IAllotedDoctor,
    password: any,
    allocated: boolean
}
export interface IAllotedDoctor{
    doctorId:string,
    doctorName:string,
    email:string,
    password:string,
    specialization:string,
}
export interface IDoctorListItem {
    value: string,
    id: number
}
export interface IDoctorRequest {
    "requestId": number,
    "doctorName": string,
    "oldNurseName": string,
    "newNurseName": string,
    "replaceReason": string,
    "acceptOrDeclineReason": null,
    "status": string,
}

export interface IAcceptReq {

    "requestId": number,
    "acceptOrDeclineReason": string,
}

export interface IDoctorHistory {
    "requestId": number,
    "doctorName": string,
    "oldNurseName": string,
    "newNurseName": string,
    "replaceReason": string,
    "acceptOrDeclineReason": string,
    "status": string
}

export interface INurseRequests {
    "requestId": number,
    "nurseName": string,
    "oldDoctorName": string,
    "newDoctorName": string,
    "replaceReason": string,
    "acceptOrDeclineReason": string,
    "status": string
}
export interface INurseHistory {
    "requestId": number,
    "nurseName": string,
    "oldDoctorName": string,
    "newDoctorName": string,
    "replaceReason": string,
    "acceptOrDeclineReason": string,
    "status": string
}
export interface IReplaceNurse {

    "doctorId": number,
    "oldNurseId": number,
    "newNurseId": number,
    "replaceReason": string

}

export interface IdoctorDetails{
    "doctorId": number,
    "doctorName": string,
    "email": string,
    "specialization":string,
    "password": string,
    "nurses":INurseResponse[]
}

